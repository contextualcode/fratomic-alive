const path = require('path');
const fs = require('fs');

class FilesystemHelper {
	copyFolderRecursiveSync(source, targetFolder) {
		// check if folder needs to be created
		if (!fs.existsSync(targetFolder)) {
			console.log(`Create directory "${targetFolder}"`);
			fs.mkdirSync(targetFolder);
		}

		// copy
		if (fs.lstatSync(source).isDirectory()) {
			fs.readdirSync(source).forEach((file) => {
				const curSource = path.join(source, file);
				if (fs.lstatSync(curSource).isDirectory()) {
					this.copyFolderRecursiveSync(curSource, path.join(targetFolder, file));
				} else {
					console.log(`Create file "${path.basename(curSource)}"`);
					let fileContent;
					if (curSource.endsWith('css')) {
						// fix path to node modules imports
						fileContent = fs.readFileSync(curSource, 'utf8')
							.split('../node_modules/')
							.join('../../node_modules/');
					} else {
						fileContent = fs.readFileSync(curSource);
					}
					fs.writeFileSync(
						path.join(targetFolder, path.basename(curSource)),
						fileContent
					);
				}
			});
		}
	}

	deleteFolderRecursive(target, deleteSelf) {
		if (fs.existsSync(target)) {
			fs.readdirSync(target).forEach((file, index) => {
				const curPath = path.join(target, file);
				if (fs.lstatSync(curPath).isDirectory()) { // recurse
					this.deleteFolderRecursive(curPath, true);
				} else { // delete file
					fs.unlinkSync(curPath);
					console.log(`Delete "${curPath}"`);
				}
			});
			if (deleteSelf) {
				fs.rmdirSync(target);
				console.log(`Delete "${target}"`);
			}
		}
	};

	createDirectory(target) {
		if (!fs.existsSync(target)) {
			fs.mkdirSync(target);
			console.log('Create folder - ' + target);
		}
	}
}

module.exports = new FilesystemHelper;
