const os = require('os');
const path = require('path');
const glob = require('glob');
const nodeExternals = require('webpack-node-externals');
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin');
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin')
const {DefinePlugin} = require('webpack');

const serverConfig = {
	entry: './src/entry-server.js',
	target: 'node',
	devtool: 'source-map',
	output: {
		libraryTarget: 'commonjs2',
	},
	externals: nodeExternals({
		whitelist: /\.css$/,
	}),
	optimization: {
		splitChunks: false,
	},
	// This is the plugin that turns the entire output of the server build
	// into a single JSON file. The default file name will be
	// `vue-ssr-server-bundle.json`
	plugins: [
		new VueSSRServerPlugin(),
		new DefinePlugin({
			"process.env.VUE_ENV": "'server'"
		}),
	],
};

const clientConfig = {
	entry: './src/entry-client.js',
	optimization: {
		runtimeChunk: {
			name: 'manifest',
		},
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendor',
					chunks: 'all',
				},
			},
		},
	},
	plugins: [
		new DefinePlugin({
			"process.env.VUE_ENV": "'client'"
		}),
		// This plugins generates `vue-ssr-client-manifest.json` in the
		// output directory.
		new VueSSRClientPlugin(),
		// remove unused CSS
		new PurgeCSSPlugin({
			paths: glob
				.sync(`${path.join(__dirname, 'src')}/**/*`, {nodir: true})
				.filter(f => f.endsWith('.vue') || f.endsWith('.html') || f.endsWith('.ts') || f.endsWith('.js')),
			defaultExtractor: function (content) {
				const contentWithoutStyleBlocks = content.replace(/<style[^]+?<\/style>/gi, '');
				return contentWithoutStyleBlocks.match(/[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+/g) || [];
			},
			fontFace: true,
			whitelist: [],
			whitelistPatterns: [
				// You can whitelist selectors based on a regular expression with whitelistPatterns.
				// /red$/ - In the example, selectors ending with red such as .bg-red will be left in the final CSS.
				/-(leave|enter|appear)(|-(to|from|active))$/, /^(?!(|.*?:)cursor-move).+-move$/, /^router-link(|-exact)-active$/, /data-v-.*/
			],
			whitelistPatternsChildren: [
				// You can whitelist selectors based on a regular expression with whitelistPatternsChildren.
				// Contrary to whitelistPatterns, it will also whitelist children of the selectors.
				// /red$/ - In the example, selectors such as red p or .bg-red .child-of-bg will be left in the final CSS.
			],
		}),
	],
};

// add your dev machine hostname here to produce source maps on local build
const localHostnames = ['it-note'];

module.exports = {
	outputDir: 'public',
	productionSourceMap: localHostnames.includes(os.hostname()), // load source maps locally
	configureWebpack: process.env.TARGET_ENV === 'server' ? serverConfig : clientConfig,
};
